Test IdP for PTE
================

v1.1

A test IdP with descriptive content, design and test login.
Test users are authenticated from static text source.

Compact, composer-based, docker-compatible, easy-to-deploy package.

This version demonstrates dual idp-s.
The IdP configured with `APP_IDP_HOST` environment variable as hostname, uses configured test auth source (default *localtest*), 

The self-test application may use one of them configured in `APP_AUTHSOURCE`. The default is 'default-sp'.

Uses via composer:
- complete simplesamlphp
- design module (Organizational design, extra attribute definitions) (optional)
- authtfaga module (2f authentication) optional (switch on for demonstration)

Local Installation from git source
-----------------------------------
1. `$ git clone`
2. (optional) `$ git pull on /modules/*`
3. `$ composer install`
4. Copy your preferred certificate files (if exist) into cert dir or create certificate manually if it has been failed during installation
5. Update environment variables (in apache config file and config/env.php) as needed.
6. Make `/web` directory web-accessible on configured hostnames (see `APP_IDP_HOST...` variables). Use config/apache-config-template.conf as template to set apache or other environment 

Docker Installation
-------------------
### Creating docker image
0. See local installation steps 1-4
7. `$ docker build -t uhi67/idptfademo .`

Settings in `env.php` are private, the will not copy into docker image. Use environment variables 
when deploying container in `docker-compose.yml` instead. 

### Uploading to private docker registry
- docker tag uhi67/idptfademo:1.0 registry.../uhi67/idptfademo
- docker push registry.../uhi67/idptfademo

### Deploying docker image in swarm
8. Create `docker-compose.yml` based on `docker-compose-template.yml`
9. Set up environment variables, ports, etc (Use all variables from env.php to docker_compose.yml as APP_*)
10. Set up reverse proxy
11. Deploy `docker-compose.yml` in your swarm environment.

A new pair of certificate is to be generated during `compose install` into `...simplesaml/cert` directory.
You may overwrite them with your certificate files, or may use these.

Environment variables
---------------------

### php container

variable name | usage
--------------|-------
APP_ENV | development (with debug) or production
APP_HTTPS | set to on if you use https *and* ssl-terminating reverse proxy
APP_TECHNICALCONTACT_NAME | technical contact in simplesaml main config. Appears in auto-generated metadata
APP_TECHNICALCONTACT_EMAIL | technical contact in simplesaml main config. Appears in auto-generated metadata
APP_SECRET_SALT | secret key for cookie validation. Use any random, unique string 
APP_ADMIN_PASSWORD | Admin password for simpleSAMLphp admin functions
APP_AUTHSOURCE | SP authentication source used for user validation, default is *default-sp*
APP_IDP_HOST | hostname for IdP (Entity id is http(s)://hostname/saml/saml2/)
APP_IDP_AUTHSOURCE | primary authsource. Default is 'tfa'
APP_PTE_COLOR | header bar color for login page
APP_PTE_TITLE | main title for login page -- translation is not supported
APP_PTE_PROMPT | Subtitle/prompt for login page -- translation is not supported
APP_PTE_HELP_TEXT | Helper text for login page -- translation is not supported
APP_CUSTOM_SOURCE | felhasználó által definiált elsődleges authentikációs forrás JSON definíciója ('custom' néven lesz az authsources-ben)
APP_CUSTOM_MODULE | felhasználó által definiált modul (repository címe)
APP_CUSTOM_KEY | RSA private key for private repository

APP_IDP_HOST is used in:
    - config/authsources.php
    - metadata/saml20-idp-hosted.php
    - metadata/saml20-idp-remote.php
    - metadata/saml20-sp-remote.php

### customizing SimpleSAMLphp configuration

SimpleSAMLphp configuration files are editable under `install/simplesaml-config-templates`. These files are copied into 
actual simpleSAMLphp config after `composer install` (or `composer reset` if only the config files has been changed).

Design module is in the `module/design` directory, no external VCS source. Change it directly for design changes.

After the changes applied you must run `composer reset` and rebuild the docker image.

Usage
-----
Test the simplesamlphp installation with 'http://idptfademo.test/saml' (use your configured base url)

Test the functionality with the login button on the start page. 

The root application itself demonstrates usage from external applications.

In this simple TFA demo, the keys of users are stored inside the docker container, so every restart of the service resets the database.
