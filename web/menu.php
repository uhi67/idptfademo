<h2>Saját adatok</h2>
<ul>
	<?php if($isAuth): ?>
		<li>Belépve mint <?= $attributes['displayName'][0] ?></li>
		<li><a href="?logout">Kilépés</a></li>
	<?php else : ?>
		<li><a href="?login&as=<?=$authsource?>" title="Teszt belépés a teszten át."><img src="img/pteid_belepes.png" alt="Belépés"/></a></li>
	<?php endif ?>
	<li><a href="showattributes.php">Saját attributumok</a></li>
</ul>

<h2>Adminisztráció</h2>
<ul>
	<li><a href="saml">IdP adminisztrációs felület</a></li>
</ul>
