<?php
require_once dirname(__DIR__).'/vendor/autoload.php';

if(!file_exists($configPath = dirname(__DIR__).'/config/config.php')) {
	echo "Configuration file '$configPath' is missing. \n Please run composer install first.";
	exit;
}
if(!file_exists($autoload = dirname(__DIR__).'/vendor/autoload.php')) {
	echo "Vendor autoload file '$autoload' is missing. \n Please run composer install first.";
	exit;
}

$config = require($configPath);
$samlconfig = $config['saml'];
$authsource = isset($samlconfig['authsource']) ? $samlconfig['authsource'] : 'default-sp';

if(class_exists('SimpleSAML_Auth_Simple')) {
	/** @noinspection PhpUndefinedClassInspection */
	$as = new SimpleSAML_Auth_Simple($authsource);
}
else {
	$as = new SimpleSAML\Auth\Simple($authsource);
}
if (array_key_exists('logout', $_REQUEST)) {
	$as->logout(SimpleSAML\Utils\HTTP::getSelfURLNoQuery());
}
if (array_key_exists('login', $_REQUEST)) {
	$as->requireAuth();
}
$isAuth = $as->isAuthenticated();
$attributes = $as->getAttributes();
$affiliation = isset($attributes['eduPersonScopedAffiliation']) ? $attributes['eduPersonScopedAffiliation'] : array();
$ou = isset($attributes['ou']) ? $attributes['ou'][0] : '';
$isIIG = $isAuth && in_array('employee@pte.hu', $affiliation) && strpos( $ou, 'KA IIG')!==false;

$la = 'hu';
/** @noinspection PhpUnhandledExceptionInspection */
$globalConfig = \SimpleSAML_Configuration::getInstance();
/** @noinspection PhpParamsInspection */
$template = new \SimpleSAML_XHTML_Template($globalConfig, 'status.php', 'attributes');
$template->setLanguage($la, false);

/**
 * Translates attribute name
 *
 * @param string $attributeName
 *
 * @return string -- translated name or original if translation is not found
 * @throws \Exception
 */
function translateAttributeName($attributeName) {
	global $template;
	return $template ? $template->getAttributeTranslation($attributeName) : $attributeName;
}

