<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html>
<head>
    <title>PTE teszt IdP</title>
    <link rel="stylesheet" type="text/css" href="css/idp.css">
	<style type="text/css">
		#content_main ul li {list-style: circle;}
	</style>
</head>
<?php
include 'saml.php';
?>
<body>
<?php
include 'header.php';
?>
<!-- felső menük helye -->
<div id="content_box">
    <div id="content">
        <div id="content_left">
            <!-- Balsávi tartalom -->
			<?php include 'menu.php'; ?>
            <!-- Balsávi tartalom vége -->
        </div>
        <div id="content_main" class="wide">
            <!-- tartalom -->
            <h1>Az Ön attributumai</h1>

            <?php if($isAuth): ?>
                <p>Ön be van lépve.</p>
            <?php else: ?>
                <p>Ön nincs belépve.</p>
            <?php endif ?>

            <p>
                Az alábbiakban azonosítás után megtekintheti saját attributumait.
                Jelen alkalmazás az ön attributumait kizárólag az önnek való megmutatás céljából kérdezi le a központi rendszerből.
                Adatai itt sem további tárolásra, sem feldolgozásra vagy harmadik rendszer felé továbbításra nem kerülnek.
            </p>
            <p>(Jelen esetben ezek a belépett teszt felhasználó attributumai)</p>


            <?php

if ($isAuth) {
	echo '<h2>Attributumok</h2>';
	echo '<dl>';

	foreach ($attributes as $name => $values) {
		/** @noinspection PhpUnhandledExceptionInspection */
		echo '<dt><b>' . translateAttributeName($name) . '</b> ('.$name.')' . '</dt>';
		echo '<dd><ul>';
		foreach ($values as $value) {
			if(is_array($value)) {
				echo '<li><ul>';
				foreach($value as $a=>$v) echo '<li>' . $a . ': '. htmlspecialchars($v) . '</li>';
				echo '</ul>';
			}
			else echo '<li>' . htmlspecialchars($value) . '</li>';
		}
		echo '</ul></dd>';
	}

	echo '</dl>';
}
?>

            <br />
        </div>
        <div id="content_right">
            <!-- Jobbsávi tartalom -->
        </div>
        <div class="cboth"></div>
    </div>
</div>
<div id="footer_box">
    <div id="footer">
        <a href="http://www.pte.hu" class="footer_logo" title="Kezdőlap"><img src="img/footer_logo.gif" alt="" /></a>
        <address><span class="addr_title">Pécsi Tudományegyetem</span><br />H-7633 Pécs, Szántó Kovács János u. 1/B.<br />+36 72 501-500 | <a href="mailto:info@pte.hu">info@pte.hu</a> | <a href="#">RSS</a></address>
    </div>
</div>

</body>
</html>
