<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html>
<head>
	<title>PTE teszt IdP</title>
	<link rel="stylesheet" type="text/css" href="css/idp.css">
	<style type="text/css">
		#content_main ul li {list-style: circle;}
	</style>
</head>
<?php

use uhi67\envhelper\EnvHelper;

include 'saml.php';
?>
<body>
<?php
	include 'header.php';

    $summary = EnvHelper::getEnv('summary', '');
?>
    <!-- felső menük helye -->
    <div id="content_box">
      <div id="content">
        <div id="content_left">
            <!-- Balsávi tartalom -->
            <?php include 'menu.php'; ?>
            <!-- Balsávi tartalom vége -->
        </div>
        <div id="content_main" class="wide">
            <!-- tartalom -->

            <h1>Kétfaktoros IdP demo</h1>
            <p>SimpleSAMLphp alapú teszt IdP integrált kétfaktoros azonosítással.</p>
            <p>A felhasználó az elsődleges hitelesítő forrást (jelszó) követően még az IdP oldalon átesik a második faktoros azonosításon is. A második faktor kódját a SimpleSAMLphp futásidejű mappában tároljuk egy SQLite adatbázisban.</p>
            <p>Jelen címlap egyúttal tesztalkalmazásként is szolgál.</p>
            <p><?= $summary ?></p>

            <h2>Tulajdonságok</h2>
            <ul>
                <li>Szabványos SAML v2 IdP</li>
                <li>Nincs szövetségi kapcsolatban</li>
                <li>Személyes adatokat nem kezel, csak teszt-accountok</li>
                <li>Integrált kétfaktors azonosítás</li>
            </ul>

            <h2>Használata</h2>

            <ul>
                <li>Entity ID: <strong>
                    <?php /** @noinspection PhpUnhandledExceptionInspection */
                    $https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : EnvHelper::getEnv('https', '');
                    $scheme = ($https == "on") ? 'https' : 'http'; ?>
                    <?= $scheme . "://".$_SERVER['HTTP_HOST'] ?>/saml/saml2/idp/metadata.php</strong></li>
            </ul>
            <p>Jelen demo IdP-hez közvetlenül nem csatlakoztathatók alkalmazások, csak mintaként szolgál hasonló rendszerek felépítéséhez.
                Az elsődleges hitelesítő forrás és a felhasználói titkos kódok tárolásának helye az <code>authsources.php</code> konfigurációs állományban módosítható.</p>
            <p>A demo forráskódja a <a href="https://bitbucket.org/uhi67/idptfademo">Bitbucketen</a>.</p>
            <p>Jelen demo egy PTE-mintájú design témát használ (design modul), ezt más intézményekben le kell cserélni a megfelelőre.</p>

            <h2>A szolgáltatott attributumok köre</h2>

            <p>Jenen demóben a teszt azonosítási forrásba bármilyen attributum beírható, egyébként az alkalmazott elsődleges hitelesítő forrás szabályai az irányadók.</p>
            <p>A hazai szövetségben szabványosított attributumok köre: <a href="https://wiki.aai.niif.hu/index.php?title=HREFAttributeSpec">HREF attributum specifikáció</a></p>

            <h2>Az alkalmazás oldalon használható termékek és útmutatók</h2>

            <p><strong>PHP</strong> környezetben a <a href="https://simplesamlphp.org/">SimpleSAMLphp</a> ajánlott. Magyar nyelvű PTE specifikus útmutató <a href="https://idp.pte.hu/spsetup.php">itt olvasható</a>.</p>
            <p><strong>Nodejs</strong> alkalmazásokhoz használható például a <a href="https://www.npmjs.com/package/passport-saml">passport-saml</a> modul. Működő <a href="https://bitbucket.org/uhi67/sixsolver/src/master/">egyszerű példa itt.</a></p>
            <p><strong>Egyéb</strong> környezetekben a <a href="https://wiki.niif.hu/index.php?title=Shibboleth_Service_Provider_(SP)">Shibboleth</a> általánosan használható termék.</p>

			<!-- tartalom vége -->
    		<br />
        </div>
       	<div id="content_right">
       		<!-- Jobbsávi tartalom -->
        </div>
        <div class="cboth"></div>
      </div>
    </div>
    <div id="footer_box">
      <div id="footer">
        <a href="http://www.pte.hu" class="footer_logo" title="Kezdőlap"><img src="img/footer_logo.gif" alt="" /></a>
        <address><span class="addr_title">Pécsi Tudományegyetem</span><br />H-7633 Pécs, Szántó Kovács János u. 1/B.<br />+36 72 501-500 | <a href="mailto:info@pte.hu">info@pte.hu</a> | <a href="#">RSS</a></address>
      </div>
    </div>

</body>
</html>

