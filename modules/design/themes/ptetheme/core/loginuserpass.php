<?php
$this->data['header'] = $this->t('{login:user_pass_header}');

if (strlen($this->data['username']) > 0) {
    $this->data['autofocus'] = 'password';
} else {
    $this->data['autofocus'] = 'username';
}
$this->includeAtTemplateBase('includes/header.php');

?>

        <?php
        if ($this->data['errorcode'] !== NULL) {
            ?>
            <div class="alert alert-danger" role="alert">
                <h4><?= $this->t('{login:error_header}') ?></h4>
                <p><b><?= $this->t('{errors:title_' . $this->data['errorcode'] . '}') ?></b></p>
                <p><?= $this->t('{errors:descr_' . $this->data['errorcode'] . '}', $this->data['errorparams']) ?></p>
            </div>
            <?php if($this->data['errorcode']=='AUTHSOURCEERROR'): ?>
		    <div class="trackidtext alert alert-warning">
		        <?php
					$session = SimpleSAML_Session::getSessionFromRequest(); 
					echo $this->t('{design:errors:report_trackid}');
				?>
		        <span class="trackid"><?= $session->getTrackID() ?></span>
		    </div>
		    <?php endif ?>
            <?php
        }
        ?>
        <div class="row">

            <div class="col-sm-6 col-xs-12 login-form">
                <div class="login-wrapper">
                    <header>
						<?php $prompt = $this->configuration->getValue('pte.prompt', (string)$this->t('{design:login:user_pass_header_2}')); ?>
                        <h4 style="break: both"><?php echo $prompt; ?></h4>
                    </header>
                    <form action="?" method="post" name="f" class="form-horizontal">

                        <div class="form-group">
                            <label for="username"
                                   class="col-xs-4 control-label"><?php echo $this->t('{login:username}'); ?> </label>

                            <?php
                            if ($this->data['forceUsername']) {
                                echo '<strong style="font-size: medium">' . htmlspecialchars($this->data['username']) . '</strong>';
                            } else {
                                echo '<input id="username" tabindex="1" type="text" name="username" maxlength="128" class="form-control" value="' . htmlspecialchars($this->data['username']) . '" placeholder="' . $this->t('{login:username}') . '"  />';
                            }
                            ?>


                        </div>

                        <div class="form-group">
                            <label for="password"
                                   class="col-xs-4 control-label"><?php echo $this->t('{login:password}'); ?> </label>

                            <input id="password" type="password" tabindex="2" name="password" maxlength="32"
                                   class="form-control" placeholder="<?php echo $this->t('{login:password}'); ?>"
                                   autocomplete="off"/>

                        </div>

                        <?php
                        if (array_key_exists('organizations', $this->data)) {
                            ?>
                            <div class="form-group">
                                <label for="password"
                                       class="col-xs-4 control-label"><?php echo $this->t('{login:organization}'); ?> </label>
                                <select class="form-control" name="organization" tabindex="3">
                                    <?php
                                    if (array_key_exists('selectedOrg', $this->data)) {
                                        $selectedOrg = $this->data['selectedOrg'];
                                    } else {
                                        $selectedOrg = NULL;
                                    }

                                    foreach ($this->data['organizations'] as $orgId => $orgDesc) {
                                        if (is_array($orgDesc)) {
                                            $orgDesc = $this->t($orgDesc);
                                        }

                                        if ($orgId === $selectedOrg) {
                                            $selected = 'selected="selected" ';
                                        } else {
                                            $selected = '';
                                        }

                                        echo '<option ' . $selected . 'value="' . htmlspecialchars($orgId) . '">' . htmlspecialchars($orgDesc) . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input id="login" class="btn btn-primary" type="submit" tabindex="4"
                                       value="<?php echo $this->t('{login:login_button}'); ?>"/>
                            </div>
                        </div>

                        <?php
                        foreach ($this->data['stateparams'] as $name => $value) {
                            echo('<input type="hidden" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars($value) . '" />');
                        }
                        ?>

                    </form>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 login-help">
                <?php
                echo('<h2>' . $this->t('{design:login:help_header}') . '</h2>');
				$help_text = $this->configuration->getValue('pte.help_text', $this->t('{design:login:help_text}'));
				echo('<p>' . $help_text . '</p>');
                if (!empty($this->data['links'])) {
                    echo '<ul class="links" style="margin-top: 2em">';
                    foreach ($this->data['links'] AS $l) {
                        echo '<li><a href="' . htmlspecialchars($l['href']) . '">' . htmlspecialchars($this->t($l['text'])) . '</a></li>';
                    }
                    echo '</ul>';
                }
                ?>
            </div>
        </div>
<?php
$this->includeAtTemplateBase('includes/footer.php');
?>