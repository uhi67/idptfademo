<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php


/**
 * Support the htmlinject hook, which allows modules to change header, pre and post body on all pages.
 */
$this->data['htmlinject'] = array(
    'htmlContentPre' => array(),
    'htmlContentPost' => array(),
    'htmlContentHead' => array(),
);


$jquery = array();
if (array_key_exists('jquery', $this->data)) $jquery = $this->data['jquery'];

if (array_key_exists('pageid', $this->data)) {
    $hookinfo = array(
        'pre' => &$this->data['htmlinject']['htmlContentPre'],
        'post' => &$this->data['htmlinject']['htmlContentPost'],
        'head' => &$this->data['htmlinject']['htmlContentHead'],
        'jquery' => &$jquery,
        'page' => $this->data['pageid']
    );

    SimpleSAML_Module::callHooks('htmlinject', $hookinfo);
}
// - o - o - o - o - o - o - o - o - o - o - o - o -


?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow"/>
    <script type="text/javascript" src="/<?php echo $this->data['baseurlpath']; ?>resources/script.js"></script>
    <title><?php
        if (array_key_exists('header', $this->data)) {
            echo $this->data['header'];
        } else {
            echo 'simpleSAMLphp';
        }
        ?></title>

    <link rel="stylesheet" type="text/css"
          href="<?php echo SimpleSAML_Module::getModuleURL('design/css/default.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo SimpleSAML_Module::getModuleURL('design/css/style.css'); ?>"/>
    <!-- Bootstrap v3.3.7 -->
    <link rel="stylesheet" type="text/css"
          href="<?php echo SimpleSAML_Module::getModuleURL('design/css/bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo SimpleSAML_Module::getModuleURL('design/css/language-picker.min.css'); ?>"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo SimpleSAML_Module::getModuleURL('design/css/flags-small.min.css'); ?>"/>
    <link rel="icon" type="image/icon"
          href="<?php echo SimpleSAML_Module::getModuleURL('design/img/pte_favicon.ico'); ?>"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script type="text/javascript"
            src="<?php echo SimpleSAML_Module::getModuleURL('design/language-picker.min.js'); ?>"></script>

    <?php

    if (!empty($jquery)) {
        $version = '1.8';
        if (array_key_exists('version', $jquery))
            $version = $jquery['version'];
		echo("<!-- $version -->\n");

		if ($version == '1.6') {
			if (isset($jquery['core']) && $jquery['core'])
				echo('<script type="text/javascript" src="/' . $this->data['baseurlpath'] . 'resources/jquery-16.js"></script>' . "\n");
		
			if (isset($jquery['ui']) && $jquery['ui'])
				echo('<script type="text/javascript" src="/' . $this->data['baseurlpath'] . 'resources/jquery-ui-16.js"></script>' . "\n");
		
			if (isset($jquery['css']) && $jquery['css'])
				echo('<link rel="stylesheet" media="screen" type="text/css" href="/' . $this->data['baseurlpath'] . 
					'resources/uitheme16/ui.all.css" />' . "\n");
		}	
        else if ($version == '1.8') {
            if (isset($jquery['core']) && $jquery['core'])
                echo('<script type="text/javascript" src="module.php/design/jquery-1.8.js"></script>' . "\n");

            if (isset($jquery['ui']) && $jquery['ui'])
                #echo('<script type="text/javascript" src="/' . $this->data['baseurlpath'] . 'resources/jquery-ui-1.8.js"></script>' . "\n");
                echo('<script type="text/javascript" src="module.php/design/jquery-ui-1.8.js"></script>' . "\n");

            if (isset($jquery['css']) && $jquery['css'])
                #echo('<link rel="stylesheet" media="screen" type="text/css" href="/' . $this->data['baseurlpath'] . 'resources/uitheme1.8/ui.all.css" />' . "\n");
                echo('<link rel="stylesheet" media="screen" type="text/css" href="/' . $this->data['baseurlpath'] . '/module.php/design/uitheme1.8/ui.all.css" />' . "\n");
        }
    }

    if (isset($this->data['clipboard.js'])) {
        echo '<script type="text/javascript" src="/' . $this->data['baseurlpath'] .
            'resources/clipboard.min.js"></script>' . "\n";
    }

    if (!empty($this->data['htmlinject']['htmlContentHead'])) {
        foreach ($this->data['htmlinject']['htmlContentHead'] AS $c) {
            echo $c;
        }
    }

    ?>

    <?php
    if (array_key_exists('head', $this->data)) {
        echo '<!-- head -->' . $this->data['head'] . '<!-- /head -->';
    }
    ?>
</head>
<?php
$onLoad = '';
if (array_key_exists('autofocus', $this->data)) {
    $onLoad .= 'SimpleSAML_focus(\'' . $this->data['autofocus'] . '\');';
}
if (isset($this->data['onLoad'])) {
    $onLoad .= $this->data['onLoad'];
}

if ($onLoad !== '') {
    $onLoad = ' onload="' . $onLoad . '"';
}

$title = $this->configuration->getValue('pte.title', 'Központi bejelentkezés');
$color = $this->configuration->getValue('pte.color');
$colorx = $color ? 'style="background-color:'.$color.'"' : '';

?>
<body<?php echo $onLoad; ?>>

<header class="branding-data row-eq-height" <?=	$colorx ?>>

    <div class="col-xs-9 col-sm-8 col-md-8 header-left">
        <div class="logo-img">
            <a href="https://pte.hu/" target="_blank" rel="home" title="Pécsi Tudományegyetem">
                <img id="header-logo" src="<?= SimpleSAML_Module::getModuleURL('design/img/PTE_emblema_90_90.png') ?>"
                     alt="Pécsi Tudományegyetem"
                     id="logo"/></a>
        </div>
        <div class="site-name-slogan">
            <p>Pécsi Tudományegyetem</p>
            <hr/>
            <p><?= $title ?></p>
        </div>

    </div>
    <div class="col-xs-3 col-sm-1 col-md-1 img_container">
        <div class="language-picker dropdown-list small">

            <?php
            $languages = $this->getLanguageList();
            $currentLang;

            $langnames = array(
                'en' => 'English',
                'de' => 'Deutsch',
                'sv' => 'Svenska',
                'fi' => 'Suomeksi',
                'es' => 'Español',
                'fr' => 'Français',
                'it' => 'Italiano',
                'cs' => 'Czech',
                'sl' => 'Slovenščina', // Slovensk
                'hr' => 'Hrvatski', // Croatian
                'hu' => 'Magyar', // Hungarian
                'pl' => 'Język polski', // Polish
                'ru' => 'Русский', // russian
                'tr' => 'Türkçe',
                'el' => 'ελληνικά',
                'ja' => '日本語',
                'zh' => '中文',
            );

            foreach ($languages AS $lang => $current) {
                if ($current) {
                    $currentLang = '<a href="javascript:void(0)" title="' . $lang . '"><i class="' . $lang . '"></i> ' . $langnames[$lang] . '</a>';
                }
            }
            ?>

            <div><?= isset($currentLang) ? $currentLang : '<a href="javascript:void(0)" title="hu"><i class="hu"></i> Magyar</a>'; ?>
                <ul>
                    <?php
                    $includeLanguageBar = true;
                    if (!empty($_POST))
                        $includeLanguageBar = FALSE;
                    if (isset($this->data['hideLanguageBar']) && $this->data['hideLanguageBar'] === TRUE)
                        $includeLanguageBar = FALSE;

                    if ($includeLanguageBar) {
                        $textarray = array();

                        foreach ($languages AS $lang => $current) {
                            if (isset($langnames[$lang])) {
                                if ($current) {
                                    $textarray[] = '<li><i class="' . $lang . '"></i> ' . $langnames[$lang] . '</li>';
                                } else {
                                    $textarray[] = '<li><a href="' . htmlspecialchars(SimpleSAML_Utilities::addURLparameter(SimpleSAML_Utilities::selfURL(), array('language' => $lang))) . '"><i class="' . $lang . '"></i> ' .
                                        $langnames[$lang] . '</a></li>';
                                }
                            }
                        }
                        echo join('', $textarray);
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 img_container slogan img-responsive">
        <img src="<?= SimpleSAML_Module::getModuleURL('design/img/PTE_szlogen.png') ?>" alt="Magyarország első egyetem"
             class="centered_image"/>
    </div>
</header>


<div id="<?= isset($this->data['sources']) ? 'sources-content' : 'content' ?>">
    <div class="container-fluid">

<?php

if (!empty($this->data['htmlinject']['htmlContentPre'])) {
    foreach ($this->data['htmlinject']['htmlContentPre'] AS $c) {
        echo $c;
    }
}
