<?php
function pte_hook_attributepresentation(&$para) {
	/*
		A cél az lett volna ezzel a hookkal, hogy az attributumok a consent oldalon névvel jelenjenek meg.
		Végül mégsem kell ehhez ide semmi érdemi, mert a config és idp_hosted konfigurációk authproc/attributemap szabályainak sorrendjével
		is el lehetett érni a kívánt hatást.  
	*/
	
	/*
		Magyarázat az eduPersonTargetedID attributumhoz 
	*/
	if(isset($para['attributes'])) {
		$attrs = $para['attributes'];
		if(isset($attrs['eduPersonTargetedID'])) {
			#$para['attributes']['eduPersonTargetedID'][] = 'Ez egy személyes adatot nem tartalmazó azonosító, mely az ön személyéhez minden szolgáltatónál állandó, de szolgáltatónként különbözik.';
		}
	}

/*
	// Ellenőrzés
	echo "<!--";
	print_r($para);
	echo "-->";
*/	
}
?>
