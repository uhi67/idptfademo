<?php
/*
	Overlay a consent/dictionaries/consent.php fájlhoz.
	Elegendő csak a megváltozott azonosító (minden nyelvű) szövegét leírni, a többi marad az eredeti.
	
	Javítás: entitások kiemelése, magyarban ':' pótlása.
*/

$lang = array(
	'consent_accept' => array (
		'no' => 'For å fullføre innloggingen må du godta at opplysningene nedenfor sendes til <b style="color:#900">SPNAME</b>.',
		'nn' => 'For å fullføra innlogginga må du godta at opplysningane under blir sende til <b style="color:#900">SPNAME</b>',
		'da' => '<b style="color:#900">SPNAME</b> kræver at nedenstående oplysninger overføres fra <b style="color:#009">IDPNAME</b>. Vil du acceptere dette?',
		'en' => '<b style="color:#900">SPNAME</b> requires that the information below is transferred by <b style="color:#009">IDPNAME</b>.',
		'de' => '<b style="color:#900">SPNAME</b> erfordert die Übertragung untenstehender Information von <b style="color:#009">IDPNAME</b>. Akzeptieren Sie das?',
		'sv' => 'Du är på väg att logga in i tjänsten <b style="color:#900">SPNAME</b>. Tjänsten kräver att informationen nedan skickas från <b style="color:#009">IDPNAME</b>. Är detta okej?',
		'fi' => 'Olet kirjautumassa palveluun <b style="color:#900">SPNAME</b>. Identiteetilähteesi henkilötietojasi palvelun tarjoajalle. Hyväksytkö tietojen siirron?',
		'es' => 'Está a punto de acceder al servicio <b style="color:#900">SPNAME</b>. El servicio requiere que la información que se muestra a continuación sea transferida desde <b style="color:#009">IDPNAME</b>. ¿Acepta esto?  ',
		'fr' => 'Vous êtes sur le point de vous connecter au service <b style="color:#900">SPNAME</b>. Lors de l\'ouverture de session, le fournisseur d\'identité enverra des informations sur votre identité à ce service. Acceptez-vous cela ?',
		'nl' => 'U gaat inloggen bij een dienst <b style="color:#900">SPNAME</b>. Tijdens het loginproces stuurt de identity provider zgn. attributen met daarin informatie over uw identiteit voor deze dienst. Bent u het daarmee eens?',
		'lb' => 'Daer sid dobai aerch um service unzemellen <b style="color:#900">SPNAME</b>. Waerend dem Login Prozess schéckt den Identity Provider Attributer, déi Informatiounen iwert aer Identitéit enthaalen. Akzeptéier daer daat?',
		'sl' => 'Pravkar se nameravate prijaviti v storitev <b style="color:#900">SPNAME</b>. Med postopkom prijave bo IdP tej storitvi posredoval atribute, ki vsebujejo informacije o vaši identiteti. Ali se s tem strinjate? ',
		'hr' => 'U tijeku je proces prijave za pristup servisu <b style="color:#900">SPNAME</b>. Servis zahtjeva da <b style="color:#009">IDPNAME</b> isporuči dolje navedene podatke. Slažete li se s time?',
		'hu' => 'Ön azonosítja magát ehhez a szolgáltatáshoz: <b style="color:#900">SPNAME</b>. <br />Az azonosítás során <b style="color:#009">IDPNAME</b> az alábbi adatokat fogja küldeni a szolgáltatásnak. Engedélyezi?',
		'pl' => '<b style="color:#900">SPNAME</b> wymaga aby poniższa informacja została przesłana.',
		'pt' => 'O serviço <b style="color:#900">SPNAME</b> necessita que a informação apresentada em baixo seja transferida.',
		'pt-br' => 'Você está prestes a acessar o serviço <b style="color:#900">SPNAME</b>. O serviço exige que as informações a seguir sejam transferidas do <b style="color:#009">IDPNAME</b>. Você aceita isso?',
		'tr' => '<b style="color:#900">SPNAME</b> aşağıdaki bilgilerin gönderilmesine ihtiyaç duyuyor.',
	),
);
