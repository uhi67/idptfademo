<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

use uhi67\envhelper\EnvHelper;

/** @noinspection PhpUnhandledExceptionInspection */
$https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : EnvHelper::getEnv('https', '');
$scheme = ($https == "on") ? 'https' : 'http';

/** @noinspection PhpUnhandledExceptionInspection */
$hostname = EnvHelper::getEnv('idp_host');

/**
 * Test IdP itself
 */
$metadata[$scheme . '://' . $hostname.'/saml/saml2/idp/metadata.php'] = array(
	'name' => array(
		'hu' => 'Pécsi Tudományegyetem - teszt azonosító szerver',
		'en' => 'Pécs University, Test IdP'
	),
	'description'          => 'Here you can login with a test account on PTE.',

	'SingleSignOnService'  => $scheme . '://' . $hostname.'/saml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => $scheme . '://' . $hostname.'/saml/saml2/idp/SingleLogoutService.php',
	'certificate' => 'idptfademo.crt',
);
