<?php
/**
 * SAML 2.0 remote SP metadata for simpleSAMLphp.
 * See: http://simplesamlphp.org/docs/trunk/simplesamlphp-reference-sp-remote
 */

use uhi67\envhelper\EnvHelper;

/** @noinspection PhpUnhandledExceptionInspection */
$https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : EnvHelper::getEnv('https', '');
$scheme = ($https == "on") ? 'https' : 'http';

/** @noinspection PhpUnhandledExceptionInspection */
$hostname = $scheme . "://" . EnvHelper::getEnv('idp_host');

// Test IdP itself
$metadata[$hostname.'/saml/module.php/saml/sp/metadata.php/default-sp'] = array (
	'AssertionConsumerService' => $hostname.'/saml/module.php/saml/sp/saml2-acs.php/default-sp',
	'SingleLogoutService' => $hostname.'/saml/module.php/saml/sp/saml2-logout.php/default-sp',
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
    'PrivacyStatementURL' => array (
		'hu' => $hostname,
    ),
  	'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
);

# Disable consent in services in the same domain
foreach($metadata as $key=>$value) {
	if(preg_match("/\.pte\.hu\//", $key)) $metadata[$key]['consent.disable'] = array('https://idp.pte.hu/saml2/idp/metadata.php');
}
