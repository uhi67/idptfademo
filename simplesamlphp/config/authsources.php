<?php

use uhi67\envhelper\EnvHelper;

/** @noinspection PhpUnhandledExceptionInspection */
$https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : EnvHelper::getEnv('https', '');
$scheme = ($https == "on") ? 'https' : 'http';
$host = $scheme . "://" . $_SERVER['HTTP_HOST'];
/** @noinspection PhpUnhandledExceptionInspection */
$hostname = EnvHelper::getEnv('idp_host');
/** @noinspection PhpUnhandledExceptionInspection */
$custom_source = EnvHelper::getEnv('custom_source', '');

/** @noinspection PhpUnhandledExceptionInspection */
$config = array(

	// This is a authentication source which handles admin authentication.
	'admin' => array(
		// The default is to use core:AdminPassword, but it can be replaced with
		// any authentication source.

		'core:AdminPassword',
	),

	// This is the test-user authentication source using external JSON webservice and local users.
	'localtest' => array(
		'exampleauth:UserPass',
		'local1:local1' => array(
			'uid' => array('local1'),
			'displayName' => 'Local Test One',
			'schacHomeOrganization' => 'pte.hu',
			'eduPersonAffiliation' => array('member', 'employee'),
			'eduPersonPrincipalName' => 'local1@idptfademo.test',
			'mail' => 'local1@idptfademo.test',
			'niifPersonOrgID' => array('LOTE1AAA'),
		),
		'admin:admin' => array(
			'uid' => array('admin'),
			'displayName' => 'Local Test One',
			'schacHomeOrganization' => 'pte.hu',
			'eduPersonAffiliation' => array('member', 'employee'),
			'eduPersonPrincipalName' => 'admin@idptfademo.test',
			'mail' => 'admin@idptfademo.test',
			'niifPersonOrgID' => array('LOAD0AAA'),
			'ou' => 'KA IIG',
		),
	),

	// This is the combined 2F authentication source. First calls 'localtest' and after the OTP authenticator.
	'tfa' => array(
		'authtfaga:authtfaga',

		'db.dsn' => 'sqlite:'.dirname(dirname(__DIR__)).'/runtime/authtf.sq3',
		'mainAuthSource' => EnvHelper::getEnv('idp_authsource', 'localtest'),
		'uidField' => 'uid', // a raw attribute provided by primary data source
		'totpIssuer' => $_SERVER['HTTP_HOST'],
	),

	// test login
	'default-sp' => array(
		'saml:SP',

		// The entity ID of this SP.
		// Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
		'entityID' => null,

		// The entity ID of the IdP this should SP should contact.
		// Can be NULL/unset, in which case the user will be shown a list of available IdPs.
		'idp' => $scheme . '://' . $hostname.'/saml/saml2/idp/metadata.php',

		'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
		'authproc' => array(
			91 => array('class' => 'core:AttributeMap', 'oid2name'),
		),
	),
);

if($custom_source!=='') {
	$config['custom'] = json_decode($custom_source, true);
}
