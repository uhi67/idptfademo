<?php
/**
 * Contents of this file will be merged into default config
 */

use uhi67\envhelper\EnvHelper;

$env = EnvHelper::getEnv('env', 'production');
if($env=='development') ini_set('display_errors', 1);

if(isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
	$protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'];
	$baseurlpath = $protocol . '://' . $_SERVER["HTTP_HOST"] . '/saml/';
}
else {
	$baseurlpath = 'saml/';
}

/** @noinspection PhpUnhandledExceptionInspection */
return array (
	'baseurlpath' => $baseurlpath,
	'loggingdir' => dirname(dirname(dirname(dirname(__DIR__)))).'/runtime/',
	'secretsalt' => EnvHelper::getEnv('secret_salt'),
	'auth.adminpassword' => EnvHelper::getEnv('admin_password'),
	'technicalcontact_name'     => EnvHelper::getEnv('technicalcontact_name'),
	'technicalcontact_email'    => EnvHelper::getEnv('technicalcontact_email'),
	'timezone' => NULL,
	'logging.level'         => \SimpleSAML\Logger::NOTICE,
	'logging.handler'       => 'file',
	'logging.facility' => defined('LOG_LOCAL5') ? constant('LOG_LOCAL5') : LOG_USER,
	'logging.processname' => 'simplesamlphp',
	'logging.logfile'		=> 'simplesamlphp.log',
	'enable.saml20-idp'		=> true,
	'session.duration'		=>  8 * (60*60), // 8 hours.
	'session.requestcache'	=>  4 * (60*60), // 4 hours
	'session.datastore.timeout' => (4*60*60), // 4 hours
	'session.state.timeout' => (60*60), // 1 hour
	'session.cookie.name' => 'idptfademosamlsession',
	'session.cookie.lifetime' => 0,
	'session.cookie.path' => '/',
	'session.cookie.domain' => NULL,
	'session.cookie.secure' => false,
	'enable.http_post' => TRUE,
	'session.authtoken.cookiename' => 'idptfademoauthtoken',
	'language.available'=> array('en', 'de', 'es', 'fi', 'fr', 'it', 'hr', 'hu', 'pl', 'ru', 'sl', 'zh' ),
	'language.rtl'		=> array('ar','dv','fa','ur','he'),
	'language.default'	=> 'hu',

	'attributes.extradictionary' => 'design:pteattributes',
	'theme.use' 	=> 'design:ptetheme',
	'pte.title'		=> EnvHelper::getEnv('pte_title', 'Kétfaktoros IdP DEMO'),
	'pte.color'		=> EnvHelper::getEnv('pte_color', '#663344'),
	'pte.help_text'	=> EnvHelper::getEnv('pte_help_text'),
	'pte.prompt'	=> EnvHelper::getEnv('pte_prompt', 'Belépés teszt felhasználóval'),

	'idpdisco.enableremember' => TRUE,
	'idpdisco.rememberchecked' => TRUE,
	'idpdisco.layout' => 'dropdown',

	/*
	 * Authentication processing filters that will be executed for all IdPs
	 * Both Shibboleth and SAML 2.0
	 */
	'authproc.idp' => array(
		1 => array(
			'class' => 'core:StatisticsWithAttribute',
			'attributename' => 'realm',
			'type' => 'saml20-idp-SSO',
		),
		5 => array(
			'class' => 'core:AttributeMap', 'oid2name',
		),

		// 10-50 idp user filters (see idp-hosted) by name

		55 => array('class' => 'core:AttributeMap', 'href-oid'),
		56 => array('class' => 'core:AttributeMap', 'name2oid'),
		// post-filters on oids
 		60 => array('class' => 'core:AttributeLimit', ),
		70 => array('class' => 'core:AttributeMap', 'oid2name'),
		71 => array('class' => 'core:AttributeMap', 'oid-href'),

		/*
		// post-filters on names
		80 => array(
	        'class'     => 'consent:Consent',
	        'store'     => 'consent:Cookie',
	        'focus'     => 'yes',
	        'checked'   => TRUE
	    ),
		*/

		// Transfer to SP by oids
		90 => array('class' => 'core:AttributeMap', 'href-oid'),
		91 => array('class' => 'core:AttributeMap', 'name2oid'),
	    99 => 'core:LanguageAdaptor',	// A felhasználó nyelvválasztásából létrehozza a preferredLanguage (2.16.840.1.113730.3.1.39) attributumot

	),
	/*
	 * Authentication processing filters that will be executed for all SPs
	 * Both Shibboleth and SAML 2.0
	 */
	'authproc.sp' => array(
		5 => array('class' => 'core:AttributeMap', 'oid2name'),
		6 => array('class' => 'core:AttributeMap', 'oid-href'),
	),

	'metadata.sources' => array(
		array('type' => 'flatfile'),
	),

	'store.type' => 'sql',
	# This must be an absolute path
	'store.sql.dsn' => 'sqlite:'.dirname(dirname(dirname(dirname(__DIR__)))).'/runtime/simplesaml.sq3',
	'store.sql.username' => NULL,
	'store.sql.password' => NULL,
	'store.sql.prefix' => 'simpleSAMLphp',

	'metadata.sign.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
	'metadata.sign.privatekey' => 'idptfademo.pem',
	'metadata.sign.privatekey_pass' => NULL,
	'metadata.sign.certificate' => 'idptfademo.crt',
);
