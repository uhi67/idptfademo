<?php
namespace uhi67\install;

#use Composer\Script\Event;
#use Composer\Installer\PackageEvent;

class Install {
	/** @noinspection PhpUndefinedClassInspection */
	/**
	 * Runned by composer after install.
	 * 2. Copies config-template if config does not exist; + runtime dir
	 * 3. Copies private simplesamlphp data
	 * 5. Creates certificate if does not exist
	 * 6. Enables modules
	 * param Event $event
	 * @throws \Exception
	 */
	public static function postInstall(/*$event*/) {
		#$composer = $event->getComposer();
		$root = dirname(__DIR__);
		$simplesamlphp = $root.'/vendor/simplesamlphp/simplesamlphp';

		// 2.1 Creates runtime dir
		if(!file_exists($root.'/runtime')) {
			mkdir($root.'/runtime');
		}
		if(!file_exists($root.'/runtime/logs')) {
			mkdir($root.'/runtime/logs');
		}

		// 3.1 copy design module
		static::rcopy($root.'/modules/design', $simplesamlphp.'/modules/design');
		// 3.2 Copy oid-href.php and href-oid.php to simplesamlphp/attributemap
		static::rcopy($root.'/simplesamlphp/attributemap', $simplesamlphp.'/attributemap');

		// 5. Certificate
		$certdir = $root.'/simplesamlphp/cert';
		$dn = array(
			"countryName" => "HU",
			"stateOrProvinceName" => "Baranya",
			"localityName" => "Pécs",
			"organizationName" => "Pécsi Tudmányegyetem",
			"organizationalUnitName" => "IIG",
			"commonName" => "Test IdP",
			"emailAddress" => "idptfademo@idptfademo.test"
		);
		if(!file_exists($certdir)) {
			mkdir($certdir);
		}
		if(!file_exists($certdir.'/idptfademo.pem')) {
			$privkey = openssl_pkey_new(array(
				'digest_alg' => 'sha256',
				"private_key_bits" => 2048,
				"private_key_type" => OPENSSL_KEYTYPE_RSA,
			));
			// Generate a certificate signing request
			$csr = openssl_csr_new($dn, $privkey, array('digest_alg' => 'sha256'));
			if(!$csr) {
				exec("openssl req -newkey rsa:2048 -new -x509 -days 3652 -nodes -config $root/install/openssl.cnf -out $certdir/idptfademo.crt -keyout $certdir/idptfademo.pem");
				if(!file_exists($certdir.'/idptfademo.pem')) {
					echo "Creating certificate is failed\n";
					echo "Enable openssl functions in php or create certificate manually as described in simplesamlphp reference.\n";
					echo "Place certificate into directory '$certdir'\n\n";
					exit(1);
				}
			}
			else {
				// Generate a self-signed cert, valid for 3652 days
				$x509 = openssl_csr_sign($csr, null, $privkey, $days = 3652, array('digest_alg' => 'sha256'));

				openssl_x509_export_to_file($x509, $certdir . '/idptfademo.crt');
				openssl_pkey_export_to_file($privkey, $certdir . '/idptfademo.pem');
			}
		}

		// 6. Enables exampleAuth module
		touch($simplesamlphp.'/modules/exampleauth/enable');
	}

	/**
	 * Resets all changes made in simplesamlphp instance
	 * @throws \Exception
	 */
	public static function reset(/*$event*/) {
		$root = dirname(__DIR__);
		$simplesamlphp = $root.'/vendor/simplesamlphp/simplesamlphp';

		static::runlink($simplesamlphp.'/modules/design');
		static::runlink($simplesamlphp.'/config');
		static::runlink($simplesamlphp.'/metadata');
		static::runlink($simplesamlphp.'/cert');
		static::runlink($root.'/runtime');
		echo "Simplesaml instance has been reset\n";

		static::postInstall();
	}

	/**
	 * Deletes multiple files recursively
	 *
	 * @param string $dir -- directory name or file
	 */
	public static function runlink($dir) {
		if (is_dir($dir)) {
			foreach (scandir($dir) as $file) {
				if ($file != "." && $file != "..")
					self::runlink("$dir/$file");
			}
		}
		else if(file_exists($dir)) {
			unlink($dir);
		}
	}


	/**
	 * copies multiple files from source to destination directory
	 *
	 * @param string $src -- source directory or file
	 * @param string $dst -- destination directory or file
	 * @param bool $overwrite
	 */
	public static function rcopy($src, $dst, $overwrite=false) {
		if (is_dir($src)) {
			if (!file_exists($dst)) mkdir($dst);
			$files = scandir($src);
			foreach ($files as $file) {
				if ($file != "." && $file != "..")
					self::rcopy("$src/$file", "$dst/$file");
			}
		}
		else if(file_exists($src) && !file_exists($dst) || $overwrite) {
			echo "Copying to $dst\n";
			copy($src, $dst);
		}
	}


	/**
	 * Calculates relative path to $to based on $from
	 *
	 * If a folder name is provided as 'to', it must be ended by '/'
	 *
	 * @author http://stackoverflow.com/users/208809/gordon
	 * @param string $from -- absolute basepath
	 * @param string $to -- absolute path to where
	 * @return string -- the relative to $to
	 */
	static function getRelativePath($from, $to) {
		#$xto = $to; $xfrom=$from;
		// some compatibility fixes for Windows paths
		$from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
		$to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
		$from = str_replace('\\', '/', $from);
		$to   = str_replace('\\', '/', $to);

		$from     = explode('/', $from);
		$to       = explode('/', $to);
		$relPath  = $to;

		foreach($from as $depth => $dir) {
			// find first non-matching dir
			//if(!array_key_exists($depth, $to)) throw new Exception('Invalid to: '.$xto);
			if(array_key_exists($depth, $to) && $dir === $to[$depth]) {
				// ignore this directory
				array_shift($relPath);
			} else {
				// get number of remaining dirs to $from
				$remaining = count($from) - $depth;
				if($remaining > 1) {
					// add traversals up to first matching dir
					$padLength = (count($relPath) + $remaining - 1) * -1;
					$relPath = array_pad($relPath, $padLength, '..');
					break;
				} else {
					if(array_key_exists(0, $relPath)) $relPath[0] = './' . $relPath[0];
				}
			}
		}
		return implode('/', $relPath);
	}
}
