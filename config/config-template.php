<?php
/**
 * Template for manually configured config.php
 * Ready-made version for docker version
 */

use uhi67\envhelper\EnvHelper;

/** @noinspection PhpUnhandledExceptionInspection */
$https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : EnvHelper::getEnv('https', '');
$scheme = ($https == "on") ? 'https' : 'http';
$host = $scheme . "://" . $_SERVER['HTTP_HOST'];
/** @noinspection PhpUnhandledExceptionInspection */
$authsource = EnvHelper::getEnv('authsource', 'default-sp');

$config = array(
	/** Configuration for SAML login module */
	'saml' => array(
		'authsource' => $authsource,								// id from simplesamlphp/config/authsources.php; Default is 'default-sp'
		'dir' => dirname(__DIR__).'/vendor/simplesamlphp/simplesamlphp/',
	),
);
return $config;
