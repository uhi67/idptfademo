<?php
return [
	'ENV' => 'development',
    # Mandatory simplesaml config variables
    'HTTPS' => 'off',
    'IDP_HOST' => '...',
    'TECHNICALCONTACT_NAME' => '...',
    'TECHNICALCONTACT_EMAIL' => '...',
    # main authentication source, may be "custom" if APP_CUSTOM_SOURCE is defined
    'IDP_AUTHSOURCE' => 'localtest',
    # Authentication source for client application
    'AUTHSOURCE' => 'default-sp',
    # Mandatory simplesaml module variables (random string)
    'SECRET_SALT' => '...',
    'ADMIN_PASSWORD' => '...',
    # Optional simplesaml module variables
    'PTE_COLOR' => '#993344',
    'PTE_TITLE' => 'Kétfaktoros IdP DEMO',
    'PTE_PROMPT' => 'Belépés kizárólag teszt felhasználónévvel',
    'PTE_HELP_TEXT' => 'Ez egy tesztelésre szolgáló bejelentkező oldal. Valódi jelszavát itt NE adja meg, itt csak a tesztelésre kiadott próbafelhasználók nevei és jelszavai használhatók.',
    'CUSTOM_SOURCE' => '{"0": "module:auth"}',
    'CUSTOM_MODULE' => '',
    'CUSTOM_KEY' => '',
    # Description for start page summary
    'SUMMARY' => 'Elsődleges azonosítási forrás: belső tesztfelhasználók',
];
